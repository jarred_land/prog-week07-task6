﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_week07_task6
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 5;
            var result = check(number);
            Console.WriteLine($"{result}");
        }

        static string check (int number) 
        {
            var output = string.Empty();

            if (number % 2 == 0)
            {
                output = "This number is even";
            }
            else
            {
                output = "This number is false";
            }
            return output;
        }
        
    }
}
